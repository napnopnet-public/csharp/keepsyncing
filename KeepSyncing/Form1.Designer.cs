﻿namespace KeepSyncing
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelGitAvailable = new System.Windows.Forms.Label();
            this.labelRobocopyString = new System.Windows.Forms.Label();
            this.textBoxRobocopyString = new System.Windows.Forms.TextBox();
            this.buttonSelectSourceFolder = new System.Windows.Forms.Button();
            this.textBoxSourceFolder = new System.Windows.Forms.TextBox();
            this.labelErrorsSourceFolder = new System.Windows.Forms.Label();
            this.buttonDestinationFolder = new System.Windows.Forms.Button();
            this.textBoxDestinationFolder = new System.Windows.Forms.TextBox();
            this.buttonSelectExcludedFolder = new System.Windows.Forms.Button();
            this.labelErrorsDestinationFolder = new System.Windows.Forms.Label();
            this.textBoxSelectExcludedFolder = new System.Windows.Forms.TextBox();
            this.buttonClearExcludedFolders = new System.Windows.Forms.Button();
            this.labelErrorsExcludedFolders = new System.Windows.Forms.Label();
            this.textBoxErrorsExcludedFolders = new System.Windows.Forms.TextBox();
            this.buttonSelectExcludedFiles = new System.Windows.Forms.Button();
            this.textBoxClearExcludedFiles = new System.Windows.Forms.Button();
            this.textBoxExcludedFiles = new System.Windows.Forms.TextBox();
            this.labelErrorsExcludedFiles = new System.Windows.Forms.Label();
            this.textBoxErrorsExcludedFiles = new System.Windows.Forms.TextBox();
            this.textBoxErrorsSelectSource = new System.Windows.Forms.TextBox();
            this.textBoxErrorsDestinationFolder = new System.Windows.Forms.TextBox();
            this.labelExcludedExtensions = new System.Windows.Forms.Label();
            this.textBoxExcludedExtensions = new System.Windows.Forms.TextBox();
            this.labelRobocopyOptions = new System.Windows.Forms.Label();
            this.textBoxRobocopyOptions = new System.Windows.Forms.TextBox();
            this.labelErrorsRobocopyOptions = new System.Windows.Forms.Label();
            this.textBoxErrorsRobocopyOptions = new System.Windows.Forms.TextBox();
            this.labelMode = new System.Windows.Forms.Label();
            this.domainUpDownMode = new System.Windows.Forms.DomainUpDown();
            this.labelSecondsInterval = new System.Windows.Forms.Label();
            this.numericUpDownSecondsInterval = new System.Windows.Forms.NumericUpDown();
            this.buttonStartLooping = new System.Windows.Forms.Button();
            this.labelLoopingMonitor = new System.Windows.Forms.Label();
            this.textBoxLoopingMonitor = new System.Windows.Forms.TextBox();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.labelErrorsExcludedExtensions = new System.Windows.Forms.Label();
            this.textBoxErrorsExcludedExtensions = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonStopLooping = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSecondsInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelGitAvailable
            // 
            this.labelGitAvailable.AutoSize = true;
            this.labelGitAvailable.Location = new System.Drawing.Point(13, 4);
            this.labelGitAvailable.MaximumSize = new System.Drawing.Size(0, 13);
            this.labelGitAvailable.Name = "labelGitAvailable";
            this.labelGitAvailable.Size = new System.Drawing.Size(24, 13);
            this.labelGitAvailable.TabIndex = 0;
            this.labelGitAvailable.Text = "plof";
            this.labelGitAvailable.Visible = false;
            // 
            // labelRobocopyString
            // 
            this.labelRobocopyString.ForeColor = System.Drawing.Color.Crimson;
            this.labelRobocopyString.Location = new System.Drawing.Point(13, 26);
            this.labelRobocopyString.Name = "labelRobocopyString";
            this.labelRobocopyString.Size = new System.Drawing.Size(86, 13);
            this.labelRobocopyString.TabIndex = 0;
            this.labelRobocopyString.Text = "Robocopy String";
            // 
            // textBoxRobocopyString
            // 
            this.textBoxRobocopyString.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxRobocopyString.Location = new System.Drawing.Point(16, 42);
            this.textBoxRobocopyString.Multiline = true;
            this.textBoxRobocopyString.Name = "textBoxRobocopyString";
            this.textBoxRobocopyString.ReadOnly = true;
            this.textBoxRobocopyString.Size = new System.Drawing.Size(753, 39);
            this.textBoxRobocopyString.TabIndex = 0;
            // 
            // buttonSelectSourceFolder
            // 
            this.buttonSelectSourceFolder.Location = new System.Drawing.Point(16, 87);
            this.buttonSelectSourceFolder.Name = "buttonSelectSourceFolder";
            this.buttonSelectSourceFolder.Size = new System.Drawing.Size(110, 35);
            this.buttonSelectSourceFolder.TabIndex = 0;
            this.buttonSelectSourceFolder.Text = "Select Source Folder";
            this.buttonSelectSourceFolder.UseVisualStyleBackColor = true;
            this.buttonSelectSourceFolder.Click += new System.EventHandler(this.buttonSelectSourceFolder_Click);
            // 
            // textBoxSourceFolder
            // 
            this.textBoxSourceFolder.Location = new System.Drawing.Point(137, 87);
            this.textBoxSourceFolder.Multiline = true;
            this.textBoxSourceFolder.Name = "textBoxSourceFolder";
            this.textBoxSourceFolder.ReadOnly = true;
            this.textBoxSourceFolder.Size = new System.Drawing.Size(632, 35);
            this.textBoxSourceFolder.TabIndex = 0;
            this.textBoxSourceFolder.TextChanged += new System.EventHandler(this.textBoxSourceFolder_TextChanged);
            // 
            // labelErrorsSourceFolder
            // 
            this.labelErrorsSourceFolder.AutoSize = true;
            this.labelErrorsSourceFolder.ForeColor = System.Drawing.Color.Crimson;
            this.labelErrorsSourceFolder.Location = new System.Drawing.Point(15, 125);
            this.labelErrorsSourceFolder.Name = "labelErrorsSourceFolder";
            this.labelErrorsSourceFolder.Size = new System.Drawing.Size(98, 13);
            this.labelErrorsSourceFolder.TabIndex = 0;
            this.labelErrorsSourceFolder.Text = "Errors source folder";
            this.labelErrorsSourceFolder.Visible = false;
            // 
            // buttonDestinationFolder
            // 
            this.buttonDestinationFolder.Location = new System.Drawing.Point(16, 183);
            this.buttonDestinationFolder.Name = "buttonDestinationFolder";
            this.buttonDestinationFolder.Size = new System.Drawing.Size(110, 35);
            this.buttonDestinationFolder.TabIndex = 0;
            this.buttonDestinationFolder.Text = "Select Destination Folder";
            this.buttonDestinationFolder.UseVisualStyleBackColor = true;
            this.buttonDestinationFolder.Click += new System.EventHandler(this.buttonDestinationFolder_Click);
            // 
            // textBoxDestinationFolder
            // 
            this.textBoxDestinationFolder.Location = new System.Drawing.Point(137, 183);
            this.textBoxDestinationFolder.Multiline = true;
            this.textBoxDestinationFolder.Name = "textBoxDestinationFolder";
            this.textBoxDestinationFolder.ReadOnly = true;
            this.textBoxDestinationFolder.Size = new System.Drawing.Size(632, 35);
            this.textBoxDestinationFolder.TabIndex = 0;
            // 
            // buttonSelectExcludedFolder
            // 
            this.buttonSelectExcludedFolder.Location = new System.Drawing.Point(16, 281);
            this.buttonSelectExcludedFolder.Name = "buttonSelectExcludedFolder";
            this.buttonSelectExcludedFolder.Size = new System.Drawing.Size(110, 35);
            this.buttonSelectExcludedFolder.TabIndex = 0;
            this.buttonSelectExcludedFolder.Text = "Select Excluded Folder";
            this.buttonSelectExcludedFolder.UseVisualStyleBackColor = true;
            this.buttonSelectExcludedFolder.Click += new System.EventHandler(this.buttonSelectExcludedFolder_Click);
            // 
            // labelErrorsDestinationFolder
            // 
            this.labelErrorsDestinationFolder.AutoSize = true;
            this.labelErrorsDestinationFolder.ForeColor = System.Drawing.Color.Crimson;
            this.labelErrorsDestinationFolder.Location = new System.Drawing.Point(14, 221);
            this.labelErrorsDestinationFolder.Name = "labelErrorsDestinationFolder";
            this.labelErrorsDestinationFolder.Size = new System.Drawing.Size(122, 13);
            this.labelErrorsDestinationFolder.TabIndex = 0;
            this.labelErrorsDestinationFolder.Text = "Errors Destination Folder";
            this.labelErrorsDestinationFolder.Visible = false;
            // 
            // textBoxSelectExcludedFolder
            // 
            this.textBoxSelectExcludedFolder.Location = new System.Drawing.Point(137, 281);
            this.textBoxSelectExcludedFolder.Multiline = true;
            this.textBoxSelectExcludedFolder.Name = "textBoxSelectExcludedFolder";
            this.textBoxSelectExcludedFolder.ReadOnly = true;
            this.textBoxSelectExcludedFolder.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxSelectExcludedFolder.Size = new System.Drawing.Size(632, 101);
            this.textBoxSelectExcludedFolder.TabIndex = 0;
            // 
            // buttonClearExcludedFolders
            // 
            this.buttonClearExcludedFolders.Location = new System.Drawing.Point(16, 322);
            this.buttonClearExcludedFolders.Name = "buttonClearExcludedFolders";
            this.buttonClearExcludedFolders.Size = new System.Drawing.Size(110, 35);
            this.buttonClearExcludedFolders.TabIndex = 0;
            this.buttonClearExcludedFolders.Text = "Clear";
            this.buttonClearExcludedFolders.UseVisualStyleBackColor = true;
            this.buttonClearExcludedFolders.Click += new System.EventHandler(this.buttonClearExcludedFolders_Click);
            // 
            // labelErrorsExcludedFolders
            // 
            this.labelErrorsExcludedFolders.AutoSize = true;
            this.labelErrorsExcludedFolders.ForeColor = System.Drawing.Color.Crimson;
            this.labelErrorsExcludedFolders.Location = new System.Drawing.Point(16, 385);
            this.labelErrorsExcludedFolders.Name = "labelErrorsExcludedFolders";
            this.labelErrorsExcludedFolders.Size = new System.Drawing.Size(118, 13);
            this.labelErrorsExcludedFolders.TabIndex = 0;
            this.labelErrorsExcludedFolders.Text = "Errors Excluded Folders";
            this.labelErrorsExcludedFolders.Visible = false;
            // 
            // textBoxErrorsExcludedFolders
            // 
            this.textBoxErrorsExcludedFolders.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxErrorsExcludedFolders.ForeColor = System.Drawing.Color.Crimson;
            this.textBoxErrorsExcludedFolders.Location = new System.Drawing.Point(16, 401);
            this.textBoxErrorsExcludedFolders.Multiline = true;
            this.textBoxErrorsExcludedFolders.Name = "textBoxErrorsExcludedFolders";
            this.textBoxErrorsExcludedFolders.ReadOnly = true;
            this.textBoxErrorsExcludedFolders.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxErrorsExcludedFolders.Size = new System.Drawing.Size(753, 35);
            this.textBoxErrorsExcludedFolders.TabIndex = 0;
            this.textBoxErrorsExcludedFolders.Visible = false;
            // 
            // buttonSelectExcludedFiles
            // 
            this.buttonSelectExcludedFiles.Location = new System.Drawing.Point(19, 476);
            this.buttonSelectExcludedFiles.Name = "buttonSelectExcludedFiles";
            this.buttonSelectExcludedFiles.Size = new System.Drawing.Size(110, 35);
            this.buttonSelectExcludedFiles.TabIndex = 0;
            this.buttonSelectExcludedFiles.Text = "Select Excluded Files";
            this.buttonSelectExcludedFiles.UseVisualStyleBackColor = true;
            this.buttonSelectExcludedFiles.Click += new System.EventHandler(this.buttonSelectExcludedFiles_Click);
            // 
            // textBoxClearExcludedFiles
            // 
            this.textBoxClearExcludedFiles.Location = new System.Drawing.Point(19, 521);
            this.textBoxClearExcludedFiles.Name = "textBoxClearExcludedFiles";
            this.textBoxClearExcludedFiles.Size = new System.Drawing.Size(110, 35);
            this.textBoxClearExcludedFiles.TabIndex = 0;
            this.textBoxClearExcludedFiles.Text = "Clear";
            this.textBoxClearExcludedFiles.UseVisualStyleBackColor = true;
            this.textBoxClearExcludedFiles.Click += new System.EventHandler(this.buttonClearExcludedFiles_Click);
            // 
            // textBoxExcludedFiles
            // 
            this.textBoxExcludedFiles.Location = new System.Drawing.Point(135, 476);
            this.textBoxExcludedFiles.Multiline = true;
            this.textBoxExcludedFiles.Name = "textBoxExcludedFiles";
            this.textBoxExcludedFiles.ReadOnly = true;
            this.textBoxExcludedFiles.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxExcludedFiles.Size = new System.Drawing.Size(632, 101);
            this.textBoxExcludedFiles.TabIndex = 0;
            // 
            // labelErrorsExcludedFiles
            // 
            this.labelErrorsExcludedFiles.AutoSize = true;
            this.labelErrorsExcludedFiles.ForeColor = System.Drawing.Color.Crimson;
            this.labelErrorsExcludedFiles.Location = new System.Drawing.Point(16, 571);
            this.labelErrorsExcludedFiles.Name = "labelErrorsExcludedFiles";
            this.labelErrorsExcludedFiles.Size = new System.Drawing.Size(105, 13);
            this.labelErrorsExcludedFiles.TabIndex = 17;
            this.labelErrorsExcludedFiles.Text = "Errors Excluded Files";
            this.labelErrorsExcludedFiles.Visible = false;
            // 
            // textBoxErrorsExcludedFiles
            // 
            this.textBoxErrorsExcludedFiles.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxErrorsExcludedFiles.ForeColor = System.Drawing.Color.Crimson;
            this.textBoxErrorsExcludedFiles.Location = new System.Drawing.Point(16, 587);
            this.textBoxErrorsExcludedFiles.Multiline = true;
            this.textBoxErrorsExcludedFiles.Name = "textBoxErrorsExcludedFiles";
            this.textBoxErrorsExcludedFiles.ReadOnly = true;
            this.textBoxErrorsExcludedFiles.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxErrorsExcludedFiles.Size = new System.Drawing.Size(753, 35);
            this.textBoxErrorsExcludedFiles.TabIndex = 0;
            this.textBoxErrorsExcludedFiles.Visible = false;
            // 
            // textBoxErrorsSelectSource
            // 
            this.textBoxErrorsSelectSource.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxErrorsSelectSource.ForeColor = System.Drawing.Color.Crimson;
            this.textBoxErrorsSelectSource.Location = new System.Drawing.Point(16, 142);
            this.textBoxErrorsSelectSource.Multiline = true;
            this.textBoxErrorsSelectSource.Name = "textBoxErrorsSelectSource";
            this.textBoxErrorsSelectSource.ReadOnly = true;
            this.textBoxErrorsSelectSource.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxErrorsSelectSource.Size = new System.Drawing.Size(753, 35);
            this.textBoxErrorsSelectSource.TabIndex = 0;
            this.textBoxErrorsSelectSource.Tag = "\"\"";
            this.textBoxErrorsSelectSource.Visible = false;
            // 
            // textBoxErrorsDestinationFolder
            // 
            this.textBoxErrorsDestinationFolder.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxErrorsDestinationFolder.ForeColor = System.Drawing.Color.Crimson;
            this.textBoxErrorsDestinationFolder.Location = new System.Drawing.Point(17, 240);
            this.textBoxErrorsDestinationFolder.Multiline = true;
            this.textBoxErrorsDestinationFolder.Name = "textBoxErrorsDestinationFolder";
            this.textBoxErrorsDestinationFolder.ReadOnly = true;
            this.textBoxErrorsDestinationFolder.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxErrorsDestinationFolder.Size = new System.Drawing.Size(752, 35);
            this.textBoxErrorsDestinationFolder.TabIndex = 0;
            this.textBoxErrorsDestinationFolder.Tag = "\"\"";
            this.textBoxErrorsDestinationFolder.Visible = false;
            // 
            // labelExcludedExtensions
            // 
            this.labelExcludedExtensions.AutoSize = true;
            this.labelExcludedExtensions.Location = new System.Drawing.Point(16, 639);
            this.labelExcludedExtensions.Name = "labelExcludedExtensions";
            this.labelExcludedExtensions.Size = new System.Drawing.Size(123, 52);
            this.labelExcludedExtensions.TabIndex = 0;
            this.labelExcludedExtensions.Text = "Excluded File extensions\r\nComma separated \r\nexpl: doc,docx,pdf,txt \r\n\r\n";
            // 
            // textBoxExcludedExtensions
            // 
            this.textBoxExcludedExtensions.Location = new System.Drawing.Point(143, 639);
            this.textBoxExcludedExtensions.Name = "textBoxExcludedExtensions";
            this.textBoxExcludedExtensions.Size = new System.Drawing.Size(624, 20);
            this.textBoxExcludedExtensions.TabIndex = 0;
            this.textBoxExcludedExtensions.TextChanged += new System.EventHandler(this.textBoxExcludedExtensions_TextChanged);
            this.textBoxExcludedExtensions.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxExcludedExtensions_KeyDown);
            this.textBoxExcludedExtensions.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxExcludedExtensions_KeyUp);
            this.textBoxExcludedExtensions.Leave += new System.EventHandler(this.textBoxExcludedExtensions_Leave);
            // 
            // labelRobocopyOptions
            // 
            this.labelRobocopyOptions.AutoSize = true;
            this.labelRobocopyOptions.Location = new System.Drawing.Point(16, 711);
            this.labelRobocopyOptions.Name = "labelRobocopyOptions";
            this.labelRobocopyOptions.Size = new System.Drawing.Size(95, 13);
            this.labelRobocopyOptions.TabIndex = 23;
            this.labelRobocopyOptions.Text = "Robocopy Options";
            // 
            // textBoxRobocopyOptions
            // 
            this.textBoxRobocopyOptions.Location = new System.Drawing.Point(143, 708);
            this.textBoxRobocopyOptions.Name = "textBoxRobocopyOptions";
            this.textBoxRobocopyOptions.ReadOnly = true;
            this.textBoxRobocopyOptions.Size = new System.Drawing.Size(624, 20);
            this.textBoxRobocopyOptions.TabIndex = 24;
            this.textBoxRobocopyOptions.TextChanged += new System.EventHandler(this.textBoxRobocopyOptions_TextChanged_1);
            this.textBoxRobocopyOptions.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxRobocopyOptions_KeyDown);
            this.textBoxRobocopyOptions.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxRobocopyOptions_KeyUp);
            this.textBoxRobocopyOptions.Leave += new System.EventHandler(this.textBoxRobocopyOptions_Leave);
            // 
            // labelErrorsRobocopyOptions
            // 
            this.labelErrorsRobocopyOptions.AutoSize = true;
            this.labelErrorsRobocopyOptions.ForeColor = System.Drawing.Color.Crimson;
            this.labelErrorsRobocopyOptions.Location = new System.Drawing.Point(140, 731);
            this.labelErrorsRobocopyOptions.Name = "labelErrorsRobocopyOptions";
            this.labelErrorsRobocopyOptions.Size = new System.Drawing.Size(125, 13);
            this.labelErrorsRobocopyOptions.TabIndex = 0;
            this.labelErrorsRobocopyOptions.Text = "Errors Robocopy Options";
            this.labelErrorsRobocopyOptions.Visible = false;
            // 
            // textBoxErrorsRobocopyOptions
            // 
            this.textBoxErrorsRobocopyOptions.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxErrorsRobocopyOptions.Location = new System.Drawing.Point(143, 747);
            this.textBoxErrorsRobocopyOptions.Multiline = true;
            this.textBoxErrorsRobocopyOptions.Name = "textBoxErrorsRobocopyOptions";
            this.textBoxErrorsRobocopyOptions.ReadOnly = true;
            this.textBoxErrorsRobocopyOptions.Size = new System.Drawing.Size(624, 20);
            this.textBoxErrorsRobocopyOptions.TabIndex = 0;
            this.textBoxErrorsRobocopyOptions.Visible = false;
            this.textBoxErrorsRobocopyOptions.TextChanged += new System.EventHandler(this.textBoxErrorsRobocopyOptions_TextChanged);
            // 
            // labelMode
            // 
            this.labelMode.AutoSize = true;
            this.labelMode.Location = new System.Drawing.Point(16, 777);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(34, 13);
            this.labelMode.TabIndex = 27;
            this.labelMode.Text = "Mode";
            // 
            // domainUpDownMode
            // 
            this.domainUpDownMode.Items.Add("Mirror");
            this.domainUpDownMode.Items.Add("Backup");
            this.domainUpDownMode.Items.Add("Custom");
            this.domainUpDownMode.Location = new System.Drawing.Point(143, 777);
            this.domainUpDownMode.Name = "domainUpDownMode";
            this.domainUpDownMode.Size = new System.Drawing.Size(120, 20);
            this.domainUpDownMode.TabIndex = 0;
            this.domainUpDownMode.Text = "Choose Mode";
            this.domainUpDownMode.SelectedItemChanged += new System.EventHandler(this.domainUpDownMode_SelectedItemChanged);
            // 
            // labelSecondsInterval
            // 
            this.labelSecondsInterval.AutoSize = true;
            this.labelSecondsInterval.Location = new System.Drawing.Point(16, 809);
            this.labelSecondsInterval.Name = "labelSecondsInterval";
            this.labelSecondsInterval.Size = new System.Drawing.Size(145, 13);
            this.labelSecondsInterval.TabIndex = 0;
            this.labelSecondsInterval.Text = "No. Seconds Repeat Interval";
            // 
            // numericUpDownSecondsInterval
            // 
            this.numericUpDownSecondsInterval.Location = new System.Drawing.Point(192, 809);
            this.numericUpDownSecondsInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSecondsInterval.Name = "numericUpDownSecondsInterval";
            this.numericUpDownSecondsInterval.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownSecondsInterval.TabIndex = 0;
            this.numericUpDownSecondsInterval.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownSecondsInterval.ValueChanged += new System.EventHandler(this.numericUpDownSecondsInterval_ValueChanged);
            // 
            // buttonStartLooping
            // 
            this.buttonStartLooping.Enabled = false;
            this.buttonStartLooping.Location = new System.Drawing.Point(450, 793);
            this.buttonStartLooping.Name = "buttonStartLooping";
            this.buttonStartLooping.Size = new System.Drawing.Size(127, 49);
            this.buttonStartLooping.TabIndex = 0;
            this.buttonStartLooping.Tag = "no-mode";
            this.buttonStartLooping.Text = "Start";
            this.buttonStartLooping.UseVisualStyleBackColor = true;
            this.buttonStartLooping.Click += new System.EventHandler(this.buttonStartLooping_Click);
            // 
            // labelLoopingMonitor
            // 
            this.labelLoopingMonitor.AutoSize = true;
            this.labelLoopingMonitor.ForeColor = System.Drawing.Color.Green;
            this.labelLoopingMonitor.Location = new System.Drawing.Point(16, 837);
            this.labelLoopingMonitor.Name = "labelLoopingMonitor";
            this.labelLoopingMonitor.Size = new System.Drawing.Size(83, 13);
            this.labelLoopingMonitor.TabIndex = 0;
            this.labelLoopingMonitor.Text = "Looping Monitor";
            // 
            // textBoxLoopingMonitor
            // 
            this.textBoxLoopingMonitor.BackColor = System.Drawing.Color.DarkBlue;
            this.textBoxLoopingMonitor.Font = new System.Drawing.Font("Noto Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxLoopingMonitor.ForeColor = System.Drawing.Color.AliceBlue;
            this.textBoxLoopingMonitor.Location = new System.Drawing.Point(19, 853);
            this.textBoxLoopingMonitor.Multiline = true;
            this.textBoxLoopingMonitor.Name = "textBoxLoopingMonitor";
            this.textBoxLoopingMonitor.ReadOnly = true;
            this.textBoxLoopingMonitor.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxLoopingMonitor.Size = new System.Drawing.Size(748, 196);
            this.textBoxLoopingMonitor.TabIndex = 0;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // labelErrorsExcludedExtensions
            // 
            this.labelErrorsExcludedExtensions.AutoSize = true;
            this.labelErrorsExcludedExtensions.ForeColor = System.Drawing.Color.Crimson;
            this.labelErrorsExcludedExtensions.Location = new System.Drawing.Point(140, 662);
            this.labelErrorsExcludedExtensions.Name = "labelErrorsExcludedExtensions";
            this.labelErrorsExcludedExtensions.Size = new System.Drawing.Size(135, 13);
            this.labelErrorsExcludedExtensions.TabIndex = 0;
            this.labelErrorsExcludedExtensions.Text = "Errors Excluded Extensions";
            this.labelErrorsExcludedExtensions.Visible = false;
            // 
            // textBoxErrorsExcludedExtensions
            // 
            this.textBoxErrorsExcludedExtensions.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxErrorsExcludedExtensions.Location = new System.Drawing.Point(143, 676);
            this.textBoxErrorsExcludedExtensions.Multiline = true;
            this.textBoxErrorsExcludedExtensions.Name = "textBoxErrorsExcludedExtensions";
            this.textBoxErrorsExcludedExtensions.ReadOnly = true;
            this.textBoxErrorsExcludedExtensions.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxErrorsExcludedExtensions.Size = new System.Drawing.Size(624, 20);
            this.textBoxErrorsExcludedExtensions.TabIndex = 35;
            this.textBoxErrorsExcludedExtensions.Visible = false;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Select Source Folder";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.Title = "Select Files";
            // 
            // buttonStopLooping
            // 
            this.buttonStopLooping.Enabled = false;
            this.buttonStopLooping.Location = new System.Drawing.Point(599, 793);
            this.buttonStopLooping.Name = "buttonStopLooping";
            this.buttonStopLooping.Size = new System.Drawing.Size(127, 49);
            this.buttonStopLooping.TabIndex = 36;
            this.buttonStopLooping.Text = "Stop";
            this.buttonStopLooping.UseVisualStyleBackColor = true;
            this.buttonStopLooping.Click += new System.EventHandler(this.buttonStopLooping_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(784, 1061);
            this.Controls.Add(this.buttonStopLooping);
            this.Controls.Add(this.labelGitAvailable);
            this.Controls.Add(this.labelRobocopyString);
            this.Controls.Add(this.textBoxRobocopyString);
            this.Controls.Add(this.buttonSelectSourceFolder);
            this.Controls.Add(this.textBoxSourceFolder);
            this.Controls.Add(this.labelErrorsSourceFolder);
            this.Controls.Add(this.textBoxErrorsSelectSource);
            this.Controls.Add(this.buttonDestinationFolder);
            this.Controls.Add(this.textBoxDestinationFolder);
            this.Controls.Add(this.labelErrorsDestinationFolder);
            this.Controls.Add(this.textBoxErrorsDestinationFolder);
            this.Controls.Add(this.buttonSelectExcludedFolder);
            this.Controls.Add(this.buttonClearExcludedFolders);
            this.Controls.Add(this.textBoxSelectExcludedFolder);
            this.Controls.Add(this.labelErrorsExcludedFolders);
            this.Controls.Add(this.textBoxErrorsExcludedFolders);
            this.Controls.Add(this.buttonSelectExcludedFiles);
            this.Controls.Add(this.textBoxClearExcludedFiles);
            this.Controls.Add(this.textBoxExcludedFiles);
            this.Controls.Add(this.labelErrorsExcludedFiles);
            this.Controls.Add(this.textBoxErrorsExcludedFiles);
            this.Controls.Add(this.labelExcludedExtensions);
            this.Controls.Add(this.textBoxExcludedExtensions);
            this.Controls.Add(this.labelErrorsExcludedExtensions);
            this.Controls.Add(this.textBoxErrorsExcludedExtensions);
            this.Controls.Add(this.labelRobocopyOptions);
            this.Controls.Add(this.textBoxRobocopyOptions);
            this.Controls.Add(this.labelErrorsRobocopyOptions);
            this.Controls.Add(this.textBoxErrorsRobocopyOptions);
            this.Controls.Add(this.labelMode);
            this.Controls.Add(this.domainUpDownMode);
            this.Controls.Add(this.labelSecondsInterval);
            this.Controls.Add(this.numericUpDownSecondsInterval);
            this.Controls.Add(this.buttonStartLooping);
            this.Controls.Add(this.labelLoopingMonitor);
            this.Controls.Add(this.textBoxLoopingMonitor);
            this.Name = "Form1";
            this.Text = "Keep Syncing";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSecondsInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelGitAvailable;
        private System.Windows.Forms.Label labelRobocopyString;
        private System.Windows.Forms.TextBox textBoxRobocopyString;
        private System.Windows.Forms.Button buttonSelectSourceFolder;
        private System.Windows.Forms.TextBox textBoxSourceFolder;
        private System.Windows.Forms.Label labelErrorsSourceFolder;
        private System.Windows.Forms.Button buttonDestinationFolder;
        private System.Windows.Forms.TextBox textBoxDestinationFolder;
        private System.Windows.Forms.Button buttonSelectExcludedFolder;
        private System.Windows.Forms.Label labelErrorsDestinationFolder;
        private System.Windows.Forms.TextBox textBoxSelectExcludedFolder;
        private System.Windows.Forms.Button buttonClearExcludedFolders;
        private System.Windows.Forms.Label labelErrorsExcludedFolders;
        private System.Windows.Forms.TextBox textBoxErrorsExcludedFolders;
        private System.Windows.Forms.Button buttonSelectExcludedFiles;
        private System.Windows.Forms.Button textBoxClearExcludedFiles;
        private System.Windows.Forms.TextBox textBoxExcludedFiles;
        private System.Windows.Forms.Label labelErrorsExcludedFiles;
        private System.Windows.Forms.TextBox textBoxErrorsExcludedFiles;
        private System.Windows.Forms.TextBox textBoxErrorsSelectSource;
        private System.Windows.Forms.TextBox textBoxErrorsDestinationFolder;
        private System.Windows.Forms.Label labelExcludedExtensions;
        private System.Windows.Forms.TextBox textBoxExcludedExtensions;
        private System.Windows.Forms.Label labelRobocopyOptions;
        private System.Windows.Forms.TextBox textBoxRobocopyOptions;
        private System.Windows.Forms.Label labelErrorsRobocopyOptions;
        private System.Windows.Forms.TextBox textBoxErrorsRobocopyOptions;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.DomainUpDown domainUpDownMode;
        private System.Windows.Forms.Label labelSecondsInterval;
        private System.Windows.Forms.NumericUpDown numericUpDownSecondsInterval;
        private System.Windows.Forms.Button buttonStartLooping;
        private System.Windows.Forms.Label labelLoopingMonitor;
        private System.Windows.Forms.TextBox textBoxLoopingMonitor;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.TextBox textBoxErrorsExcludedExtensions;
        private System.Windows.Forms.Label labelErrorsExcludedExtensions;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonStopLooping;
    }
}

