﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeepSyncing
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            String gitInfo = CommandOutput("git --version");
            String gitInfol = gitInfo.ToLower();
            bool gitAvailable = false;
            if (gitInfol.StartsWith("git version"))
            {
                gitAvailable = true;
                gitInfo = "Git found on your system:: " + gitInfo;
            }
            else
            {
                gitInfo = "No git found on your system";
            }
            StateDataSource stateDateSourceProg = new StateDataSource(gitInfo, gitAvailable, "Robocopy etc. /L");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        public static string CommandOutput(string command, string workingDirectory = null)
        {// https://stackoverflow.com/questions/26167387/run-git-commands-from-a-c-sharp-function
            try
            {
                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + command);
                procStartInfo.RedirectStandardError = procStartInfo.RedirectStandardInput = procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;

                if (null != workingDirectory)
                {
                    procStartInfo.WorkingDirectory = workingDirectory;
                }
                else
                {
                    procStartInfo.WorkingDirectory = Directory.GetCurrentDirectory();
                }

                Process proc = new Process();
                proc.StartInfo = procStartInfo;
                proc.Start();

                StringBuilder sb = new StringBuilder();
                
                proc.OutputDataReceived += delegate (object sender, DataReceivedEventArgs e)
                {
                    sb.AppendLine(e.Data);
                };
                proc.ErrorDataReceived += delegate (object sender, DataReceivedEventArgs e)
                {
                    if(e.Data != null)
                    {
                        sb.AppendLine("error stream");
                    }
                    sb.AppendLine(e.Data);
                    if (e.Data != null)
                    {
                        sb.AppendLine("end error stream");
                    }
                    
                };

                proc.BeginOutputReadLine();
                proc.BeginErrorReadLine();
                proc.WaitForExit();
                return sb.ToString();
            }
            catch (Exception objException)
            {
                return $"Error in command: {command}, {objException.Message}";
            }
        }

        public static String RemoveEmptyLines(String lines)
        {
            return Regex.Replace(lines, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
        }

    }
}
