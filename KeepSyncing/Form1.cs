﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace KeepSyncing
{
    public partial class Form1 : Form
    {
        StateDataSource stateDataSource;
        
        List<String> lines = new List<String>();
        public Form1()
        {
            String gitInfo = Program.CommandOutput("git --version");
            String gitInfol = gitInfo.ToLower();
            bool gitAvailable = false;
            if (gitInfol.StartsWith("git version"))
            {
                gitAvailable = true;
                gitInfo = "Git found on your system:: " + gitInfo;
            }
            else
            {
                gitInfo = "No git found on your system";
            }
            this.stateDataSource = new StateDataSource(gitInfo, gitAvailable, "");
            
            this.stateDataSource.SourceFolderPath = "";
            
            

            InitializeComponent();
            
            BindingSource stateBindingSource = new BindingSource();
            
            stateBindingSource.DataSource = this.stateDataSource;
            labelGitAvailable.DataBindings.Add("Text", stateBindingSource, "GitVersion");

            textBoxRobocopyString.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            textBoxRobocopyString.DataBindings.Add("Text", stateBindingSource, "RobocopyString");
            
            textBoxSourceFolder.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            textBoxSourceFolder.DataBindings.Add("Text", stateBindingSource, "SourceFolderPath");

            textBoxDestinationFolder.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            textBoxDestinationFolder.DataBindings.Add("Text", stateBindingSource, "DestinationFolderPath");

            textBoxSelectExcludedFolder.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            textBoxSelectExcludedFolder.DataBindings.Add("Text", stateBindingSource, "ExcludedFolders");

            textBoxExcludedFiles.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            textBoxExcludedFiles.DataBindings.Add("Text", stateBindingSource, "ExcludedFiles");
            
            textBoxRobocopyOptions.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            textBoxRobocopyOptions.DataBindings.Add("Text", stateBindingSource, "RoboCopyOptions");

            numericUpDownSecondsInterval.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
            numericUpDownSecondsInterval.DataBindings.Add("Value", stateBindingSource, "LoopInterval");
        }

        
        private async void DoSomeWork(int milliseconds)
        {
            int loopCount = 0;
            while (this.stateDataSource.RunLoop == true)
            {
                loopCount += 1;
                if (this.stateDataSource.RunLoop == false)// should be useless now
                {
                    
                    break;
                }
                

                // report completion
                bool uiMarshal = textBoxLoopingMonitor.InvokeRequired;
                String msg = String.Format("Some work iter completed in {0} ms. on {1}UI thread\n",
                                   milliseconds, uiMarshal ? String.Empty : "non-");
                String msg2 = this.stateDataSource.RobocopyString;
                
                //string roboOutput = Program.CommandOutput(this.stateDataSource.RobocopyString + " /L"); debug
                string roboOutput = Program.CommandOutput(this.stateDataSource.RobocopyString);
                
                if (uiMarshal)
                {
                    textBoxLoopingMonitor.Invoke(new Action(() => {

                        
                        if (textBoxLoopingMonitor.TextLength > 6 * roboOutput.Length)
                        {
                            //Console.WriteLine(" multi of 6 reached!!!! shorten from the top");
                            int shortenWith = msg2.Length + roboOutput.Length + 2; // 2 newlines
                            String CurrentContent = textBoxLoopingMonitor.Text;
                            textBoxLoopingMonitor.Clear();
                            textBoxLoopingMonitor.AppendText(CurrentContent.Substring(shortenWith));


                        }

                        //textBoxLoopingMonitor.AppendText(Environment.NewLine + msg);
                        textBoxLoopingMonitor.AppendText(Environment.NewLine + msg2);
                        textBoxLoopingMonitor.AppendText(Environment.NewLine + roboOutput);

                        //Console.WriteLine(textBoxLoopingMonitor.TextLength);
                        

                    }));
                }
                //else
                //{
                //    //textBoxLoopingMonitor.Lines = lines.ToArray();
                //    //textBoxLoopingMonitor.AppendText("AAAAAAAAAAAAAAAA");
                //}
                await Task.Delay(milliseconds);

            }
            
            // report completion
            bool uiMarshalend = textBoxLoopingMonitor.InvokeRequired;
            String msgend = String.Format("Some work completed in {0} ms. on {1}UI thread\n",
                               milliseconds, uiMarshalend ? String.Empty : "non-");
            

            //lines.Add(msgend);
            
            if (uiMarshalend)
            {
                //textBoxLoopingMonitor.Invoke(new Action(() => { textBoxLoopingMonitor.Lines = lines.ToArray(); }));
                textBoxLoopingMonitor.Invoke(new Action(() => { textBoxLoopingMonitor.AppendText("Loop ended"); }));

                //here adapt the buttons etc. 
                buttonStartLooping.Invoke(new Action(() => { buttonStartLooping.Text = "Start Robocopy" + Environment.NewLine + "Loop"; }));
                buttonStartLooping.Invoke(new Action(() => { buttonStartLooping.Tag = "robo-start-loop"; }));
                buttonStartLooping.Invoke(new Action(() => { buttonStartLooping.Enabled = true; }));

                
                buttonStopLooping.Invoke(new Action(() => { buttonStopLooping.Text = "Cancel"; }));
                buttonStopLooping.Invoke(new Action(() => { buttonStopLooping.Enabled = true; }));
                buttonStopLooping.Invoke(new Action(() => { buttonStopLooping.Visible = true; }));
                buttonStopLooping.Invoke(new Action(() => { buttonStopLooping.Tag = "robo-cancel-start-loop"; }));
                
            }
            
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            String roboCopyInfo = Program.CommandOutput("Robocopy /?");

            if (roboCopyInfo.StartsWith("error stream" + Environment.NewLine + "'Robocopy' is not recognized as an internal or external command"))
            {
                // Error message to loop monitor
                textBoxLoopingMonitor.Text = "Robocopy not found on your system, sorry, not functionality left";
                // and blocking of all fields
                blockInput(true);
            }
            roboCopyInfo = "";
        }

        private bool testRobocopyString()
        {
            string robocopyTestString = this.stateDataSource.RobocopyString + " /L  & echo \"cmd exit code: \" %ErrorLevel% ";//; echo %ErrorLevel%
            string result = Program.CommandOutput(robocopyTestString);
            
            string searchPattern = "(?m)^ERROR\\s:\\s.*$";
            
            if (Regex.IsMatch(result, searchPattern))
            {
                MatchCollection matches = Regex.Matches(result, searchPattern);
                
                textBoxLoopingMonitor.Text = "";
                textBoxLoopingMonitor.AppendText(matches[0].ToString());
                if(matches[0].ToString().Contains("Invalid Parameter #1 "))
                {
                    textBoxLoopingMonitor.AppendText(Environment.NewLine);
                    textBoxLoopingMonitor.AppendText("Please supply a source folder path");
                }
                if (matches[0].ToString().Contains("Invalid Parameter #2 "))
                {
                    textBoxLoopingMonitor.AppendText(Environment.NewLine);
                    textBoxLoopingMonitor.AppendText("Please supply a destination folder path");
                }
                textBoxLoopingMonitor.AppendText(Environment.NewLine);
                textBoxLoopingMonitor.AppendText(Environment.NewLine);
                textBoxLoopingMonitor.AppendText("---------------- test report ----------------");
                textBoxLoopingMonitor.AppendText(Environment.NewLine);
                textBoxLoopingMonitor.AppendText(result);
                textBoxLoopingMonitor.AppendText(Environment.NewLine);
                textBoxLoopingMonitor.AppendText("---------------- end test report ----------------");
                textBoxLoopingMonitor.SelectionStart = 0;
                textBoxLoopingMonitor.ScrollToCaret();
                
                return false;
            }
            else
            {;
                textBoxLoopingMonitor.Text = "";
                textBoxLoopingMonitor.AppendText(result);
                
                return true;
            }

        }

        private void buttonSelectSourceFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Choose Source Folder";
            
            DialogResult result = folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                // do some test on the newly chosen path
                Dictionary<String, String> returnedDict = this.stateDataSource.TestSourceFolderPath( folderBrowserDialog1.SelectedPath );
                
                if(returnedDict["status"] == "ERROR")
                {
                    String extraString = "";
                    if (returnedDict["errorCode"] == "00")
                    {
                        extraString = ", try again";
                    }
                    if (returnedDict["errorCode"] == "01")
                    {
                        extraString = ", try again";
                    }
                    if (returnedDict["errorCode"] == "02")
                    {
                        extraString = ", try again";
                    }
                    if (returnedDict["errorCode"] == "03")
                    {
                        extraString = ", try again";
                    }
                    if (returnedDict["errorCode"] == "04")
                    {
                        extraString = "";
                        textBoxErrorsDestinationFolder.Text = returnedDict["status"] + " " + returnedDict["errorCode"] + ": " + returnedDict["message"];
                        textBoxErrorsDestinationFolder.Visible = true;
                        labelErrorsDestinationFolder.Visible = true;
                        textBoxErrorsDestinationFolder.Tag = returnedDict["errorCode"];
                        textBoxSourceFolder.Text = folderBrowserDialog1.SelectedPath;
                        textBoxErrorsDestinationFolder.Tag = returnedDict["errorCode"];
                    }
                    if (returnedDict["errorCode"] == "05")
                    {
                        extraString = ", try again";
                        
                    }
                    textBoxErrorsSelectSource.Text = returnedDict["status"] + " " + returnedDict["errorCode"] + ": " + returnedDict["message"] + extraString;
                    textBoxErrorsSelectSource.Tag = returnedDict["errorCode"];
                    textBoxErrorsSelectSource.Visible = true;
                    labelErrorsSourceFolder.Visible = true;
                }      
                else if (returnedDict["status"] == "OK")
                {
                    textBoxErrorsSelectSource.Text = "";
                    textBoxErrorsSelectSource.Visible = false;
                    labelErrorsSourceFolder.Visible = false;
                    textBoxSourceFolder.Text = folderBrowserDialog1.SelectedPath;
                    textBoxErrorsSelectSource.Tag = "";
                    
                    if (textBoxErrorsDestinationFolder.Tag.ToString() == "04" || textBoxErrorsDestinationFolder.Tag.ToString() == "14")
                    {
                        textBoxErrorsDestinationFolder.Tag = "";
                        textBoxErrorsDestinationFolder.Text = "";
                        textBoxErrorsDestinationFolder.Visible = false;
                        labelErrorsDestinationFolder.Visible = false;
                        
                    }
                }
                
                renewExcludedFolders(this.textBoxSelectExcludedFolder.Text, "");
                string[] emptyArray = new string[] { }; // empty string
                
                renewExcludedFiles(this.textBoxExcludedFiles.Text, emptyArray);
                textBoxRobocopyString.Text = this.stateDataSource.BuildRobocopyCommand();
            }
        }

        private void textBoxSourceFolder_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonDestinationFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Choose Destination Folder";

            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                // do some test on the newly chosen path
                Dictionary<String, String> returnedDict = this.stateDataSource.TestDestinationFolderPath(folderBrowserDialog1.SelectedPath);

                if (returnedDict["status"] == "ERROR")
                {
                    String extraString = "";
                    if (returnedDict["errorCode"] == "10")
                    {
                        extraString = ", try again";
                    }
                    if (returnedDict["errorCode"] == "11")
                    {
                        extraString = ", try again";
                    }
                    if (returnedDict["errorCode"] == "12")
                    {
                        extraString = ", try again";
                    }
                    if (returnedDict["errorCode"] == "13")
                    {
                        extraString = ", try again";
                    }
                    if (returnedDict["errorCode"] == "14")
                    {
                        extraString = "";

                        textBoxErrorsSelectSource.Text = returnedDict["status"] + " " + returnedDict["errorCode"] + ": " + returnedDict["message"];
                        textBoxErrorsSelectSource.Visible = true;
                        labelErrorsSourceFolder.Visible = true;
                        textBoxErrorsSelectSource.Tag = returnedDict["errorCode"];
                        textBoxDestinationFolder.Text = folderBrowserDialog1.SelectedPath;
                        textBoxErrorsSelectSource.Tag = returnedDict["errorCode"];
                        
                    }
                    if (returnedDict["errorCode"] == "15")
                    {
                        extraString = ", try again";
                    }
                    textBoxErrorsDestinationFolder.Text = returnedDict["status"] + " " + returnedDict["errorCode"] + ": " + returnedDict["message"] + extraString;
                    textBoxErrorsDestinationFolder.Tag = returnedDict["errorCode"];
                    textBoxErrorsDestinationFolder.Visible = true;
                    labelErrorsDestinationFolder.Visible = true;
                    
                }

                else if (returnedDict["status"] == "OK")
                {
                    textBoxErrorsDestinationFolder.Text = "";
                    textBoxErrorsDestinationFolder.Visible = false;
                    labelErrorsDestinationFolder.Visible = false;
                    textBoxDestinationFolder.Text = folderBrowserDialog1.SelectedPath;
                    textBoxErrorsDestinationFolder.Tag = "";
                    
                    if (textBoxErrorsSelectSource.Tag.ToString() == "04" || textBoxErrorsSelectSource.Tag.ToString() == "14")
                    {
                        textBoxErrorsSelectSource.Tag = "";
                        textBoxErrorsSelectSource.Text = "";
                        textBoxErrorsSelectSource.Visible = false;
                        labelErrorsSourceFolder.Visible = false;
                         
                    }
                }
                
                renewExcludedFolders(this.textBoxSelectExcludedFolder.Text, "");
                string[] emptyArray = new string[] { }; // empty string
                renewExcludedFiles(this.textBoxExcludedFiles.Text, emptyArray);
                textBoxRobocopyString.Text = this.stateDataSource.BuildRobocopyCommand();
            }
            //
        }


        private void evaluateExcludedFolders()
        {
            Console.WriteLine(this.textBoxSelectExcludedFolder.Text);
            string textToClean = this.textBoxSelectExcludedFolder.Text;
            string cleanedText = "";

            textToClean = textToClean.Replace("[same_as_source_ignored]", "");
            Console.WriteLine(textToClean);

            textToClean = textToClean.Replace("[same_as_destination_ignored]", "");
            Console.WriteLine(textToClean);

            textToClean = textToClean.Replace("[outside_source_ignored]", "");
            Console.WriteLine(textToClean);

            textToClean = textToClean.Replace("[inside_destination_ignored]", "");
            Console.WriteLine(textToClean);

            cleanedText = textToClean;

            
            string newString = "";
            string[] seperators = new string[] { Environment.NewLine };
            
            string[] tempLines = cleanedText.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
            string tempPath = "";
            string prefix = "";
            foreach (string str in tempLines)
            {
                prefix = "";
                if (str.Contains("[nested_destination]"))
                {
                    if(newString.Length > 0) { newString += Environment.NewLine; }
                    newString += str;
                    continue;

                }
                // if source is not empty
                if(this.stateDataSource.SourceFolderPath != "")
                {
                    // if source is the same as str
                    if(this.stateDataSource.SourceFolderPath == str)
                    {
                        prefix += "[same_as_source_ignored]";
                    }
                    // outside source

                    // test if excluded folder is not inside source folder
                    if (!(str.Length > this.stateDataSource.SourceFolderPath.Length
                        && str.Substring(0, this.stateDataSource.SourceFolderPath.Length) == this.stateDataSource.SourceFolderPath))
                    // if (tempPath == this.stateDataSource.DestinationFolderPath)
                    {
                        prefix += "[outside_source_ignored]";
                    }
                    
                }
                // if destination is not empty
                if(this.stateDataSource.DestinationFolderPath != "")
                {
                    if(this.stateDataSource.DestinationFolderPath == str)
                    {
                        prefix += "[same_as_destination_ignored]";
                    }

                    if(str.Length > this.stateDataSource.DestinationFolderPath.Length && 
                        str.Substring(0, this.stateDataSource.DestinationFolderPath.Length) == this.stateDataSource.DestinationFolderPath)
                    {
                        prefix += "[inside_destination_ignored]";
                    }
                }
                
                if(newString.Length > 0) { newString += Environment.NewLine; }
                newString += prefix + str;
                
                this.textBoxSelectExcludedFolder.Text = newString;
                
                this.textBoxErrorsExcludedFolders.Tag = "";
                this.textBoxErrorsExcludedFolders.Visible = false;
                this.labelErrorsExcludedFolders.Visible = false;
            }
        }

        private void renewExcludedFolders(string excludedFolders, string choosenFolder)
        {
            ReturnTypeExcludedFolders statusInfo = this.stateDataSource.UpdateExcludedFolders(excludedFolders, choosenFolder);
            
            this.textBoxSelectExcludedFolder.Text = statusInfo.excludedFoldersText;
            
            this.textBoxErrorsExcludedFolders.Text = statusInfo.errorsExcludedFoldersText;
            
            this.labelErrorsExcludedFolders.Visible = statusInfo.errorsVisible;
            this.textBoxErrorsExcludedFolders.Visible = statusInfo.errorsVisible;

        }

        private void buttonSelectExcludedFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Choose an Excluded Folder";
            
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                renewExcludedFolders(this.textBoxSelectExcludedFolder.Text, folderBrowserDialog1.SelectedPath);
                
                textBoxRobocopyString.Text = this.stateDataSource.BuildRobocopyCommand();
            }

        }

        private void buttonClearExcludedFolders_Click(object sender, EventArgs e)
        {
            if (textBoxSelectExcludedFolder.Text.Contains("[nested_destination]"))
            {
                string[] seperators = new string[] { Environment.NewLine };
                string[] tempLines = textBoxSelectExcludedFolder.Text.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                textBoxSelectExcludedFolder.Text = "";
                foreach (string str in tempLines)
                {
                    if (str.Contains("[nested_destination]"))
                    {
                        if (textBoxSelectExcludedFolder.Text.Length > 0)
                        {
                            textBoxSelectExcludedFolder.AppendText(Environment.NewLine);
                        }
                        textBoxSelectExcludedFolder.AppendText(str);
                    }
                }
            }
            else
            {
                textBoxSelectExcludedFolder.Text = "";
            }

            textBoxErrorsExcludedFolders.Text = "";
            textBoxErrorsExcludedFolders.Tag = "";
            textBoxErrorsExcludedFolders.Visible = false;
            labelErrorsExcludedFolders.Visible = false;
            textBoxRobocopyString.Text = this.stateDataSource.BuildRobocopyCommand();
        }


        private void renewExcludedFiles(string excludedFiles, string[] choosenFiles)
        {
            ReturnTypeExcludedFiles statusInfo = this.stateDataSource.UpdateExcludedFiles(this.textBoxExcludedFiles.Text, choosenFiles);
            
            this.textBoxExcludedFiles.Text = statusInfo.excludedFilesText;
            
            this.textBoxErrorsExcludedFiles.Text = statusInfo.errorsExcludedFilesText;
            
            this.labelErrorsExcludedFiles.Visible = statusInfo.errorsVisible;
            this.textBoxErrorsExcludedFiles.Visible = statusInfo.errorsVisible;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSelectExcludedFiles_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Select 1 or more files to exclude";
            //string tempText = this.textBoxExcludedFiles.Text;
            
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                renewExcludedFiles(this.textBoxExcludedFiles.Text, openFileDialog1.FileNames);

                textBoxRobocopyString.Text = this.stateDataSource.BuildRobocopyCommand();

            }
        }

        private void buttonClearExcludedFiles_Click(object sender, EventArgs e)
        {
            textBoxExcludedFiles.Text = "";
            textBoxErrorsExcludedFiles.Text = "";

            textBoxErrorsExcludedFiles.Tag = "";
            textBoxErrorsExcludedFiles.Visible =false;
            labelErrorsExcludedFiles.Visible = false;
            
            textBoxRobocopyString.Text = this.stateDataSource.BuildRobocopyCommand();
        }

        private void domainUpDownMode_SelectedItemChanged(object sender, EventArgs e)
        {
            this.stateDataSource.ModeItemString = domainUpDownMode.SelectedItem.ToString();
            String tempStr = domainUpDownMode.SelectedItem.ToString();
            
            if ((domainUpDownMode.SelectedItem.ToString().Contains("Mirror") ||
                 domainUpDownMode.SelectedItem.ToString().Contains("Backup") ||
                 domainUpDownMode.SelectedItem.ToString().Contains("Custom")) &&
                 !domainUpDownMode.SelectedItem.ToString().Contains("Git"))
            {
                buttonStartLooping.Text = "Test" + Environment.NewLine + "Robocopy String";
                buttonStartLooping.Tag = "robo-mode";
                buttonStopLooping.Tag = "robo-mode";
                buttonStopLooping.Text = "Stop";
                buttonStopLooping.Enabled = false;
                buttonStartLooping.Visible = true;
                buttonStartLooping.Enabled = true;
            }
            else if(domainUpDownMode.SelectedItem.ToString().Contains("Git"))
            {
                // for now
                buttonStartLooping.Text = "Start";
                buttonStartLooping.Tag = "git-mode";
                //buttonStartLooping.Visible = false;
                buttonStartLooping.Enabled = false;
            }
            else //should never happen
            {
                buttonStartLooping.Text = "Start";
                buttonStartLooping.Tag = "no-mode";
                //buttonStartLooping.Visible = false;
                buttonStartLooping.Enabled = false;
            }
            this.SyncRoboCopyOptions(tempStr);//?? what does it now?
            
            textBoxRobocopyString.Text = this.stateDataSource.BuildRobocopyCommand();
        }

        private void SyncRoboCopyOptions(String options)
        {
            textBoxRobocopyOptions.ReadOnly = true;
            
            textBoxRobocopyOptions.Text = this.stateDataSource.Mode[options];
            
            if (domainUpDownMode.SelectedItem.ToString() == "Custom")
            {
                textBoxRobocopyOptions.ReadOnly = false;
                //TODO: test robocopy with /L dry run //only with custom ????
            }

            this.stateDataSource.RoboCopyOptions = this.stateDataSource.Mode[options];
        }

        private void textBoxRobocopyOptions_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBoxRobocopyOptions_KeyUp(object sender, KeyEventArgs e)
        {
            //Console.WriteLine(e.KeyCode);
            
            //!!!! https://docs.microsoft.com/en-us/dotnet/api/system.windows.input.key?view=netcore-3.1
            // en niet 
            // https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.keys?view=netcore-3.1
            // https://stackoverflow.com/questions/29365878/how-do-i-compare-pressed-key-values-in-keydown-event
            // value 6 has two names Return and Enter
            
            if (e.KeyCode == Keys.Return)
            {
                //TODO: test robocopy with /L dry run //only with custom
                
                textBoxRobocopyString.Text =  this.stateDataSource.BuildRobocopyCommand();
            }
            
            
        }

        private void textBoxRobocopyOptions_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBoxRobocopyOptions_KeyDown(object sender, KeyEventArgs e)
        {
            
            //e.Handled = true;
            //e.SuppressKeyPress = true;
            if (e.KeyCode == Keys.Return)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void textBoxRobocopyOptions_Leave(object sender, EventArgs e)
        {
            
            //TODO: test robocopy with /L dry run
            
            textBoxRobocopyString.Text = this.stateDataSource.BuildRobocopyCommand();
        }

        private void textBoxExcludedExtensions_KeyDown(object sender, KeyEventArgs e)
        {

        }


        private void handleExcludedExtensions()
        {

            // first add trailing comma
            //char[] invalidPathChars = Path.GetInvalidPathChars();//werkt niet goed??
            string invalCharPattern = "[\\\\/:*?\"<>|]";
            string errorMessages = "";
            if (Regex.IsMatch(textBoxExcludedExtensions.Text,invalCharPattern))
            {
                MatchCollection matches = Regex.Matches(textBoxExcludedExtensions.Text, invalCharPattern);
                foreach(Match match in matches)
                {
                    if(errorMessages.Length > 0)
                    {
                        errorMessages += Environment.NewLine;
                    }

                    errorMessages += "character " + match.ToString() + " not allowed, and removed";
                    //Console.WriteLine("character " + match.ToString() + " not allowed, and removed");
                }
            }
            string cleanedExcludedExtenstions = Regex.Replace(textBoxExcludedExtensions.Text, invalCharPattern, "");
            
            if(errorMessages == "")
            {
                textBoxErrorsExcludedExtensions.Visible = false;
                textBoxErrorsExcludedExtensions.Text = "";
                labelErrorsExcludedExtensions.Visible = false;

            }
            else
            {
                textBoxErrorsExcludedExtensions.Text = errorMessages;
                textBoxErrorsExcludedExtensions.Visible = true;
                labelErrorsExcludedExtensions.Visible = true;
            }
            textBoxExcludedExtensions.Text = cleanedExcludedExtenstions;
            this.stateDataSource.ExcludedExtensions = cleanedExcludedExtenstions;

            textBoxRobocopyString.Text =  this.stateDataSource.BuildRobocopyCommand();
            
            
            ////see http://www.java2s.com/Tutorials/CSharp/System.IO/Path/C_Path_GetInvalidFileNameChars.htm

        }


        private void textBoxExcludedExtensions_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                
                //TODO: test robocopy with /L dry run
                
                handleExcludedExtensions();
                
            }
        }

        private void textBoxExcludedExtensions_Leave(object sender, EventArgs e)
        {

            handleExcludedExtensions();
            
        }


        private void blockInput(bool block)
        {
            bool notBlock = !block;
            //if(block == true)
            //{
            buttonSelectSourceFolder.Enabled = notBlock;
            buttonDestinationFolder.Enabled = notBlock;
            buttonSelectExcludedFolder.Enabled = notBlock;
            buttonClearExcludedFolders.Enabled = notBlock;
            buttonSelectExcludedFiles.Enabled = notBlock;
            textBoxClearExcludedFiles.Enabled = notBlock;
            textBoxExcludedExtensions.Enabled = notBlock;
            if(block == true)
            {
                textBoxRobocopyOptions.ReadOnly = block;
            }
            if(block == false)
            {
                if(domainUpDownMode.SelectedItem.ToString() == "Custom")
                {
                    textBoxRobocopyOptions.ReadOnly = false;
                }
            }
                //textBoxRobocopyOptions.ReadOnly = block;
            domainUpDownMode.Enabled = notBlock;
            numericUpDownSecondsInterval.Enabled = notBlock;
        }

       // private void buttonStartLooping_Click(object sender, EventArgs e)
        public async void buttonStartLooping_Click(object sender, EventArgs e)

        {
            if(buttonStartLooping.Tag.ToString() == "robo-mode")
            {
                blockInput(true);

                bool success = testRobocopyString();
                if (!success)
                {
                    //unlock input fields
                    blockInput(false);
                }else
                {
                    buttonStartLooping.Text = "Start Robocopy" + Environment.NewLine + "Loop";
                    buttonStartLooping.Tag = "robo-start-loop";
                    buttonStartLooping.Enabled = true;

                    buttonStopLooping.Text = "Cancel";
                    buttonStopLooping.Enabled = true;
                    buttonStopLooping.Visible = true;
                    buttonStopLooping.Tag = "robo-cancel-start-loop";
                }
            }

            else if(buttonStartLooping.Tag.ToString() == "robo-start-loop")
            {
                //change stop loop naar "Stop" and tag robo-stop-loop en enabled true
                //button start enabled false tag robo-loop-running
                buttonStartLooping.Enabled = false;
                buttonStartLooping.Text = "Looping";
                buttonStartLooping.Tag = "robo-looping";

                buttonStopLooping.Enabled = true;
                buttonStopLooping.Text = "Stop";
                buttonStopLooping.Tag = "robo-stop-looping";
                textBoxLoopingMonitor.Text = "";
                textBoxLoopingMonitor.AppendText("Loop started" + Environment.NewLine);
                this.stateDataSource.RunLoop = true;
                //textBoxLoopingMonitor.AppendText("Simulating work on non-UI thread.");
                //await Task.Run(() => DoSomeWork(1000));
                await Task.Run(() => DoSomeWork(this.stateDataSource.LoopInterval * 1000));
                //Console.WriteLine("AAAAAAAAAAAAAAAAAAAAAAAAAA NOT NOT NOT awaited stuff");
            } 

            //Console.WriteLine(this.stateDataSource.ModeItemString);//name of the mode
                                                                   // eerste tekst op de button moet misschien "test loop" zijn?
                                                                   //Bepaal eerst wat de Mode is : 
                                                                   //als geen git
                                                                   // test eerst de robocopystring met /L
                                                                   // maak daarvoor een functie
            
        }

        private void textBoxLoopingMonitor_VisibleChanged(object sender, EventArgs e)
        {
            textBoxLoopingMonitor.SelectionStart = textBoxLoopingMonitor.TextLength;
            textBoxLoopingMonitor.ScrollToCaret();
        }

        private void buttonStopLooping_Click(object sender, EventArgs e)
        {
            //this.stateDataSource.RunLoop = false;//debug

            if(buttonStopLooping.Tag.ToString() == "robo-cancel-start-loop")
            {
                blockInput(false);
                buttonStopLooping.Enabled = false;
                buttonStopLooping.Text = "Stop";
                buttonStopLooping.Tag = "";
                
                buttonStartLooping.Text = "Test" + Environment.NewLine + "Robocopy String";
                buttonStartLooping.Tag = "robo-mode";
            } else if(buttonStopLooping.Tag.ToString() == "robo-stop-looping"){
                this.stateDataSource.RunLoop = false;
                buttonStopLooping.Enabled = false;
                buttonStartLooping.Text = "Stopping";
                
            }

        }

        private void textBoxRobocopyOptions_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBoxExcludedExtensions_TextChanged(object sender, EventArgs e)
        {
            //string typedCharacter = textBoxExcludedExtensions.Text.Substring(textBoxExcludedExtensions.Text.Length - 1);
            ;
        }

        private void numericUpDownSecondsInterval_ValueChanged(object sender, EventArgs e)
        {
            //Debug:
            //Console.WriteLine(numericUpDownSecondsInterval.Value);
            //Console.WriteLine(this.stateDataSource.LoopInterval);
            //for (int i = 0; i < 20; i++)
            //{
            //    Console.WriteLine(this.stateDataSource.LoopInterval);
            //}


        }

        private void textBoxErrorsRobocopyOptions_TextChanged(object sender, EventArgs e)
        {

        }
    }
}


/**
 * Het databinding: en dan heen en weer, vooral/voolopig met ui en object::
 * 
 *  https://docs.microsoft.com/en-us/dotnet/api/system.windows.forms.bindingsource.resetbindings?view=netcore-3.1
 *  https://docs.microsoft.com/en-us/dotnet/desktop/winforms/controls/reflect-data-source-updates-in-a-wf-control-with-the-bindingsource?view=netframeworkdesktop-4.8
 *  https://www.youtube.com/watch?v=9f7IoKQIraQ
 * 
 *  Maak een classe die als type van een object dient aan met public properties
 * In designer een datasource aanmaken, ofwel in Data Sources paneel, ofwel bv een textBox te selecteren en in properties -paneel
 *  blokje Date, (Data Bindings), Text. Dan wordt er door designer een "bindings source" voor de class aangemaakt. De bindingsource
 *  heeft een property Datasource: daar voer je een instantie van de class in, bv. bij de form-load-event-functie: 
 *  
 *  In het geval van textBox wordt nu de waarde van de textbox automatisch naar het object weggeschreven, maar niet andersom: als 
 *  het object een verandering ondergaat bij een property, dan wordt de tekst in de tesktbox niet veranderd. 
 *  Met de .ResetBindings(false) wordt de opdracht gegeven het object opnieuw te lezen zodat de textbox wordt aangepast. 
 *  
 *  //Het moet geloof ik ook mogelijk zijn om de class zodanig te schrijven dat die een event genereert zodanig dat
 *  dat resetten niet meer hoeft. 
 * https://docs.microsoft.com/en-us/dotnet/desktop/winforms/controls/raise-change-notifications--bindingsource?view=netframeworkdesktop-4.8
 */
