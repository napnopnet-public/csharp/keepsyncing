﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KeepSyncing
{
    public class StateDataSource : INotifyPropertyChanged
    {
        public StateDataSource(string gitVersion, bool gitPresent, string robocopyString)
        {
            GitVersion = gitVersion;
            //GitPresent = gitPresent;
            GitPresent = false;
            RobocopyString = robocopyString;
            SourceFolderPath = "";
            DestinationFolderPath = "";
            ExcludedFolders = "";
            ExcludedFiles = "";
            ExcludedExtensions = "";
            RoboCopyOptions = "/L";
            Mode = new Dictionary<string, string>
            {
                { "Mirror", "/MIR /MT" },
                { "Backup", "/E /MT" },
                { "Custom", "" }
                //{ "Custom", "" },
                //{ "To Git", "" },
                //{ "Mirror by Git", "" },
                //{ "Backup by Git", "" },
                //{ "Stage to Git", "" }
            };
            LoopInterval = 5;
            RunLoop = false;
        }

        public String GitVersion { get; set; }
        public bool GitPresent { get; set; }


        //public RobocopyPlan RobocopyPlan { get; set; }
        

        // RobocopyString
        private String robocopyString;
        public String RobocopyString //{ get; set; }
        {
            get
            {
                return this.robocopyString;
            }
            set
            {
                if (value != this.robocopyString) {
                    this.robocopyString = value;
                    NotifyPropertyChanged();
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public String SourceFolderPath { get; set; }

        public String DestinationFolderPath { get; set; }

        public String ExcludedFolders { get; set; }

        public String ExcludedFiles { get; set; }

        public String ExcludedExtensions { get; set; }

        public String RoboCopyOptions { get; set; }

        public Dictionary<String, String> Mode { get; set; }
        public String ModeItemString { get; set; }

        public int LoopInterval { get; set; }

        

        public Boolean RunLoop { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //public void UpdateStuff()
        //{
        //    Console.WriteLine("updating stuff");
            
        //}

        private void CoordinateSourceAndDestination(String sourceFolderPath, String destinationFolder)
        {
           
        }

        /// <summary>
        /// method does some tests on a path as a String parameter and returns a Dictionary .
        /// </summary>
        /// <param name="folder">the path  (String)</param>
        /// <returns>a Dictionary containing errors, error codes, messages, status</returns>
        public Dictionary<String, String> TestAFolder(String folder)
        {
            Dictionary<String, String> returnDict = new Dictionary<String, String>();
            returnDict.Add("status", "");
            returnDict.Add("errorCode", "");
            returnDict.Add("message", "");
            try
            {
                DirectoryInfo dirInf = new DirectoryInfo(folder);
                // test if path exists
                if (dirInf.Exists != true)
                {
                    returnDict["errorCode"] = "1";
                    System.ArgumentException argExp = new System.ArgumentException(folder + " does not exists");
                    throw argExp;
                }
                // test if path is directory
                if (!dirInf.Attributes.ToString().Contains("Directory"))
                {
                    returnDict["errorCode"] = "2";
                    System.ArgumentException argExp = new System.ArgumentException(folder + " is not a folder");
                    throw argExp;
                }
            }
            catch (Exception excp)
            {
                returnDict["status"] = "ERROR";
                if (returnDict["errorCode"] == "")
                {
                    returnDict["errorCode"] = "0";
                    returnDict["message"] = "unexpected: " + excp.Message;
                }
                else
                {
                    returnDict["message"] = excp.Message;
                }

                return returnDict;
            }

            returnDict["status"] = "OK";
            return returnDict;
        }

        public Dictionary<String,String> TestAFile(String fileToTest)
        {
            Dictionary<String, String> returnDict = new Dictionary<String, String>();
            returnDict.Add("status", "");
            returnDict.Add("errorCode", "");
            returnDict.Add("message", "");
            try
            {
                FileInfo fileInf = new FileInfo(fileToTest);
                 
                // test if path exists
                if (fileInf.Exists != true)
                // if (fileInf.Exists == true) //debug
                {
                    returnDict["errorCode"] = "1";
                    System.ArgumentException argExp = new System.ArgumentException(fileToTest + " does not exist");
                    throw argExp;
                }
                // test if path is a file
                //if (fileInf.Attributes.ToString().Contains("Directory") )
                if (fileInf.Attributes.HasFlag(FileAttributes.Directory))
                // if (!fileInf.Attributes.HasFlag(FileAttributes.Directory))// debug
                {
                    returnDict["errorCode"] = "2";
                    System.ArgumentException argExp = new System.ArgumentException(fileToTest + " is a directory, not a file");
                    throw argExp;
                }
            }
            catch (Exception excp)
            {
                returnDict["status"] = "ERROR";
                if (returnDict["errorCode"] == "")
                {
                    returnDict["errorCode"] = "0";
                    returnDict["message"] = "unexpected: " + excp.Message;
                }
                else
                {
                    returnDict["message"] = excp.Message;
                }

                return returnDict;

            }

            returnDict["status"] = "OK";
            return returnDict;
        }
        public Dictionary<String, String> TestSourceFolderPath(String sourceFolderPath)
        {
            Dictionary<String, String> testedFolderDict = TestAFolder(sourceFolderPath);
            
            Dictionary<String, String> returnDict =  new Dictionary<String, String>();
            returnDict.Add("status", "ERROR");
            returnDict.Add("errorCode", "");
            returnDict.Add("message", "");
            if (testedFolderDict["status"] == "ERROR")
            {
                testedFolderDict["erroCode"] = "0" + testedFolderDict["erroCode"];
                return testedFolderDict;
            }
            try
            {
                DirectoryInfo dirInf = new DirectoryInfo(sourceFolderPath);
                
                // see if this.DestinationFolderPath is set
                if (this.DestinationFolderPath != "")
                {
                    // test if source is not the same as destination
                    if (this.DestinationFolderPath == sourceFolderPath)
                    {
                        returnDict["errorCode"] = "03";
                        System.ArgumentException argExp = new System.ArgumentException(sourceFolderPath + " is the same folder as the destination folder, try again");
                        throw argExp;
                    }

                    // test if destination folder is inside source folder
                    if (this.DestinationFolderPath.Length > sourceFolderPath.Length
                        && this.DestinationFolderPath.Substring(0, sourceFolderPath.Length) == sourceFolderPath)
                    {
                        returnDict["errorCode"] = "04";
                        System.ArgumentException argExp = new System.ArgumentException("Destination folder " + this.DestinationFolderPath + "is inside source folder "+sourceFolderPath + ", destination folder is added to the excluded folders list");
                        throw argExp;
                    }

                    // test if source folder is inside destination folder
                    if (this.DestinationFolderPath.Length < sourceFolderPath.Length
                        && sourceFolderPath.Substring(0, this.DestinationFolderPath.Length) == this.DestinationFolderPath)
                    {
                        returnDict["errorCode"] = "05";
                        System.ArgumentException argExp = new System.ArgumentException("Source folder " + sourceFolderPath + "is inside destination folder " + this.DestinationFolderPath + ", please try again");
                        throw argExp;
                    }
                }   
            }
            catch (Exception excp)
            {
               // excp.Message
                returnDict["status"] = "ERROR";
                returnDict["message"] = " for " + sourceFolderPath + ": "+  excp.Message;
                
                return returnDict;
            }

            returnDict["status"] = "OK";

            return returnDict;
        }

        public Dictionary<String, String> TestDestinationFolderPath(String destinationFolderPath)
        {
            Dictionary<String, String> testedFolderDict = TestAFolder(destinationFolderPath);
            
            Dictionary<String, String> returnDict = new Dictionary<String, String>();
            returnDict.Add("status", "ERROR");
            returnDict.Add("errorCode", "");
            returnDict.Add("message", "");
            if (testedFolderDict["status"] == "ERROR")
            {
                testedFolderDict["erroCode"] = "1" + testedFolderDict["erroCode"];
                return testedFolderDict;
            }

            try
            {
                DirectoryInfo dirInf = new DirectoryInfo(destinationFolderPath);
                System.Security.AccessControl.DirectorySecurity accesContrl = dirInf.GetAccessControl();
                
                // see if this.SourceFolderPath is set
                if (this.SourceFolderPath != "")
                {
                    // test if destination is not the same as source
                    if (this.SourceFolderPath == destinationFolderPath)
                    {
                        returnDict["errorCode"] = "13";
                        System.ArgumentException argExp = new System.ArgumentException(destinationFolderPath + " is the same folder as the source folder, try again");
                        throw argExp;
                    }

                    // test if destination folder is inside source folder
                    if (destinationFolderPath.Length > this.SourceFolderPath.Length
                        && destinationFolderPath.Substring(0, this.SourceFolderPath.Length) == this.SourceFolderPath)
                    {
                        returnDict["errorCode"] = "14";
                        System.ArgumentException argExp = new System.ArgumentException("Destination folder " + destinationFolderPath + "is inside source folder " + this.SourceFolderPath + ", destination folder is added to the excluded folders list");
                        throw argExp;
                    }

                    // test if source folder is inside destination folder
                    if (destinationFolderPath.Length < this.SourceFolderPath.Length
                        && this.SourceFolderPath.Substring(0, destinationFolderPath.Length) == destinationFolderPath)
                    {
                        returnDict["errorCode"] = "15";
                        System.ArgumentException argExp = new System.ArgumentException("Source folder " + this.SourceFolderPath + "is inside destination folder " + destinationFolderPath + ", please try again");
                        throw argExp;
                    }
                }
            }
            catch (Exception excp)
            {
                // excp.Message
                returnDict["status"] = "ERROR";
                returnDict["message"] = " for " + destinationFolderPath + ": " + excp.Message;

                return returnDict;
            }

            returnDict["status"] = "OK";

            return returnDict;
        }

        public Dictionary<String, String> TestExcludedFolderPath(String excludedFolderPath)
        {

            Dictionary<String, String> testedFolderDict = TestAFolder(excludedFolderPath);

            Dictionary<String, String> returnDict = new Dictionary<String, String>();
            returnDict.Add("status", "ERROR");
            returnDict.Add("errorCode", "");
            returnDict.Add("message", "");
            if (testedFolderDict["status"] == "ERROR")
            {
                testedFolderDict["errorCode"] = "2" + testedFolderDict["errorCode"];
                return testedFolderDict;
            }
            try
            {
                DirectoryInfo dirInf = new DirectoryInfo(excludedFolderPath);

                // see if this.SourceFolderPath is set
                if (this.SourceFolderPath != "")
                {
                    // test if source folder is not the same as excluded folder
                    if(this.SourceFolderPath == excludedFolderPath)
                    {
                        returnDict["errorCode"] = "23";
                        System.ArgumentException argExp = new System.ArgumentException(excludedFolderPath + " is the same folder as the source folder");
                        throw argExp;
                    }

                    

                    // test if excluded folder is not inside source folder
                    if (!(excludedFolderPath.Length > this.SourceFolderPath.Length
                        && excludedFolderPath.Substring(0, this.SourceFolderPath.Length) == this.SourceFolderPath))
                    {
                        returnDict["errorCode"] = "25";
                        System.ArgumentException argExp = new System.ArgumentException("excluded folder " + excludedFolderPath + "is not inside source folder " + this.SourceFolderPath + ", excluded folder is labeled");
                        throw argExp;
                    }

                }

                if(this.DestinationFolderPath != "")
                {
                    // test if excluded folder is the same as  destination folder
                    if (this.DestinationFolderPath == excludedFolderPath)
                    {
                        returnDict["errorCode"] = "24";
                        
                        System.ArgumentException argExp = new System.ArgumentException("excluded folder " + excludedFolderPath + "is the same as the destination folder " + this.DestinationFolderPath + ", excluded folder is labeled");
                        throw argExp;
                    }

                }
            }
            catch (Exception excp)
            {
                //excp.Message
                returnDict["status"] = "ERROR";
                returnDict["message"] = " for " + excludedFolderPath + ": " + excp.Message;

                return returnDict;
            }

            returnDict["status"] = "OK";

            return returnDict;
        }

        public Dictionary<String, String> TextExcludedFilePath(String excludedFilePath)
        {
            Dictionary<String, String> testedFileDict = TestAFile(excludedFilePath);

            Dictionary<String, String> returnDict = new Dictionary<String, String>();
            returnDict.Add("status", "ERROR");
            returnDict.Add("errorCode", "");
            returnDict.Add("message", "");
            if (testedFileDict["status"] == "ERROR")
            {
                testedFileDict["errorCode"] = "3" + testedFileDict["errorCode"];
                return testedFileDict;
            }
            try
            {
                FileInfo fileToTest = new FileInfo(excludedFilePath);
                // see if this.SourceFolderPath is set
                if (this.SourceFolderPath != "")
                {
                    // test if file is not inside source folder
                    if (!(excludedFilePath.Length > this.SourceFolderPath.Length
                        && excludedFilePath.Substring(0, this.SourceFolderPath.Length) == this.SourceFolderPath))
                    {
                        returnDict["errorCode"] = "35";
                        System.ArgumentException argExp = new System.ArgumentException("excluded File " + excludedFilePath + "is not inside source folder " + this.SourceFolderPath + ", excluded file is labeled");
                        throw argExp;
                    }
                }

                // see if this.DestinationFolderPath is set
                if (this.DestinationFolderPath != "")
                {
                    // test if file is inside destination folder
                    if (excludedFilePath.Length > this.DestinationFolderPath.Length
                        && excludedFilePath.Substring(0, this.DestinationFolderPath.Length) == this.DestinationFolderPath)
                    {
                        returnDict["errorCode"] = "36";
                        System.ArgumentException argExp = new System.ArgumentException("excluded File " + excludedFilePath + "is  inside destination folder " + this.DestinationFolderPath + ", excluded file is labeled");
                        throw argExp;
                    }

                }
            }
            catch (Exception excp)
            {
                //excp.Message
                returnDict["status"] = "ERROR";
                returnDict["message"] = " for " + excludedFilePath + ": " + excp.Message;

                return returnDict;

            }
            returnDict["status"] = "OK";

            return returnDict;
        }

        public ReturnTypeExcludedFiles UpdateExcludedFiles(string exclFiles, string[] choosenFiles)
        {
            /* TODO: remove all labels from tempText, to cleanedText// DONE
           * then in a foreach add every chosen file path to cleanedText if not already //DONE
           * then transform cleanedText to string array cleanedLines and 
           * iterate over lcleanedLines and do tests and add labels and replace textbox text with resulting tempText //DONE
           * 
           */
            // remove all labels from tempText
            // https://docs.microsoft.com/en-us/dotnet/standard/base-types/regular-expression-language-quick-reference
            // (?m) switches to multi line modus: then ^ indicates start of a line in the string instead of start of the string
            string pattern = @"(?m)^\[[^]]*]";
            string replacement = @"";
            string cleanedText = "";
            string newText = "";
            string foundErrors = "";
            string lastTag = "";
            bool errorsVisible = true;
            cleanedText = Regex.Replace(exclFiles, pattern, replacement);
            
            // iterate over paths of files chosen in this file choose round

            //foreach (String choosenFile in openFileDialog1.FileNames)
            foreach (String choosenFile in choosenFiles)
            {
                // add path only if it is not there already
                if (!cleanedText.Contains(choosenFile))
                {
                    if (cleanedText.Length > 0)
                    {
                        cleanedText += Environment.NewLine;
                    }
                    cleanedText += choosenFile;
                }

            }

            // transform cleanedText from string to string array 
            string[] seperators = new string[] { Environment.NewLine };
            string[] cleanedLines = cleanedText.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
            
            foreach (String exclFile in cleanedLines)
            {
                // do some tests on the newly chosen path
                //Dictionary<String, String> returnedDict = this.stateDataSource.TextExcludedFilePath(exclFile);
                Dictionary<String, String> returnedDict = this.TextExcludedFilePath(exclFile);

                if (returnedDict["status"] == "ERROR")
                // if (returnedDict["status"] != "ERROR") // debug
                {
                    String extraString = "";

                    if (returnedDict["errorCode"] == "30")
                    // if (!(returnedDict["errorCode"] == "30"))// debug
                    {
                        extraString = ", try again";

                    }
                    if (returnedDict["errorCode"] == "31")
                    // if (!(returnedDict["errorCode"] == "31"))// debug
                    {
                        extraString = ", try again";

                    }
                    if (returnedDict["errorCode"] == "32")
                    // if (!(returnedDict["errorCode"] == "32"))// debug
                    {
                        extraString = ", try again";

                    }
                    if (returnedDict["errorCode"] == "35")
                    // if (!(returnedDict["errorCode"] == "30"))// debug
                    {
                        extraString = ", label ignored";
                        if (newText.Length > 0)
                        {
                            newText += Environment.NewLine;
                        }
                        newText += "[outside_source_ignored]" + exclFile;

                        //if (this.textBoxExcludedFiles.Text.Length > 0)
                        //{
                        //    this.textBoxExcludedFiles.AppendText(Environment.NewLine);
                        //}
                        //this.textBoxExcludedFiles.AppendText("[outside_source_ignored]" + exclFile);

                    }
                    if (returnedDict["errorCode"] == "36")
                    // if (!(returnedDict["errorCode"] == "30"))// debug
                    {
                        extraString = ", label ignored";
                        if (newText.Length > 0)
                        {
                            newText += Environment.NewLine;
                        }
                        newText += "[inside_destination_ignored]" + exclFile;

                        //if (this.textBoxExcludedFiles.Text.Length > 0)
                        //{
                        //    this.textBoxExcludedFiles.AppendText(Environment.NewLine);
                        //}
                        //this.textBoxExcludedFiles.AppendText("[inside_destination_ignored]" + exclFile);

                    }

                    if (foundErrors.Length > 0)
                    {
                        foundErrors += Environment.NewLine;
                    }
                    foundErrors += returnedDict["status"] + " " + returnedDict["errorCode"] + ": " + returnedDict["message"] + extraString;

                    lastTag = returnedDict["errorCode"];// ??
                    errorsVisible = true;


                    //if (textBoxErrorsExcludedFiles.Text.Length > 0)
                    //{
                    //    textBoxErrorsExcludedFiles.AppendText(Environment.NewLine);
                    //}
                    //textBoxErrorsExcludedFiles.AppendText(returnedDict["status"] + " " + returnedDict["errorCode"] + ": " + returnedDict["message"] + extraString);
                    //textBoxErrorsExcludedFiles.Tag = returnedDict["errorCode"];
                    //textBoxErrorsExcludedFiles.Visible = true;
                    //labelErrorsExcludedFiles.Visible = true;

                }
                else if (returnedDict["status"] == "OK")
                {
                    lastTag = "";// ??
                    errorsVisible = true;


                    if (newText.Length > 0)
                    {
                        newText += Environment.NewLine;
                    }
                    newText += exclFile;

                    //if (this.textBoxExcludedFiles.Text.Length > 0)
                    //{
                    //    this.textBoxExcludedFiles.AppendText(Environment.NewLine);
                    //}
                    //this.textBoxExcludedFiles.AppendText(exclFile);

                }
            }
            if (foundErrors.Length == 0)
            {
                errorsVisible = false;
                lastTag = "";
            }
            ReturnTypeExcludedFiles returnObject = new ReturnTypeExcludedFiles();
            returnObject.excludedFilesText = newText;
            returnObject.errorsExcludedFilesText = foundErrors;
            returnObject.lastTag = lastTag;
            returnObject.errorsVisible = errorsVisible;
            return returnObject;

        }

        public ReturnTypeExcludedFolders UpdateExcludedFolders(string exclFolders, string choosenFolder)
        //public void UpdateExcludedFolders(string exclFolders, string choosenFolder)
        {

            //string patternNestedNewLine = @"(?m)^\[nested_destination].*" + Environment.NewLine;
            //string patternNestedNewLine = @"(?m)^\[nested_destination].*$" ;
            //string patternNoNewLine= @"(?m)^\[nested_destination].*$";
            string patternNested = @"(?m)^\[nested_destination].*$";
                                            
            string replacementNested = "";

            string pattern = @"(?m)^\[[^]]*]";
            string replacement = "";

            string cleanedText = "";
            string newText = "";
            string foundErrors = "";
            string lastTag = "";
            bool errorsVisible = true;

            string escapedText = "";

            //if (exclFolders.Contains(Environment.NewLine))
            // {
            // escapedText = Regex.Replace(exclFolders, patternNestedNewLine, replacementNested);
            //} else
            //{
            //escapedText = Regex.Replace(exclFolders, patternNested, replacementNested);
            // }
            
            escapedText = Regex.Replace(exclFolders, patternNested, replacementNested);

            
           // escapedText = Regex.Replace(exclFolders, patternNestedNewLine, replacementNested);
            cleanedText = Regex.Replace(escapedText, pattern, replacement);
            
            // add path only if it is not there already
            if (!cleanedText.Contains(choosenFolder))
            {
                if (cleanedText.Length > 0)
                {
                    cleanedText += Environment.NewLine;
                }
                cleanedText += choosenFolder;
            }

            // transform cleanedText from string to string array 
            string[] seperators = new string[] { Environment.NewLine };
            string[] cleanedLines = cleanedText.Split(seperators, StringSplitOptions.RemoveEmptyEntries);

            foreach (String exclFolder in cleanedLines)
            {
                // do some tests on the path
                Dictionary<String, String> returnedDict = this.TestExcludedFolderPath(exclFolder);

                if (returnedDict["status"] == "ERROR")
                {
                    String extraString = "";

                    if (returnedDict["errorCode"] == "20")
                    // if (!(returnedDict["errorCode"] == "20"))// debug
                    {
                        extraString = ", try again";

                    }
                    if (returnedDict["errorCode"] == "21")
                    // if (!(returnedDict["errorCode"] == "21"))// debug
                    {
                        extraString = ", try again";

                    }
                    if (returnedDict["errorCode"] == "22")
                    // if (!(returnedDict["errorCode"] == "22"))// debug
                    {
                        extraString = ", try again";

                    }
                    if (returnedDict["errorCode"] == "23")
                    // if (!(returnedDict["errorCode"] == "23"))// debug
                    {
                        extraString = ", try again";

                    }

                    if (returnedDict["errorCode"] == "24")
                    // if (!(returnedDict["errorCode"] == "24"))// debug
                    {
                        extraString = ", label ignored";
                        if (newText.Length > 0)
                        {
                            newText += Environment.NewLine;
                        }
                        newText += "[same_as_destination_ignored]" + exclFolder;

                    }
                    //24 met label folder is the same as destination folder


                    if (returnedDict["errorCode"] == "25")
                    // if (!(returnedDict["errorCode"] == "25"))// debug
                    {
                        extraString = ", label ignored";
                        if (newText.Length > 0)
                        {
                            newText += Environment.NewLine;
                        }
                        newText += "[outside_source_ignored]" + exclFolder;

                    }
                    //25 met label folder is not inside source folder

                    if (foundErrors.Length > 0)
                    {
                        foundErrors += Environment.NewLine;
                    }
                    foundErrors += returnedDict["status"] + " " + returnedDict["errorCode"] + ": " + returnedDict["message"] + extraString;

                    lastTag = returnedDict["errorCode"];// ??
                    errorsVisible = true;

                }
                else if (returnedDict["status"] == "OK")
                {
                    lastTag = "";// ??
                    errorsVisible = true;

                    if (newText.Length > 0)
                    {
                        newText += Environment.NewLine;
                    }
                    newText += exclFolder;

                }
            }
            if (foundErrors.Length == 0)
            {
                errorsVisible = false;
                lastTag = "";
            }
            if(this.SourceFolderPath != "" && 
                this.DestinationFolderPath != "" && 
                this.DestinationFolderPath.Length > this.SourceFolderPath.Length &&
                this.DestinationFolderPath.Substring(0, this.SourceFolderPath.Length) == this.SourceFolderPath)
            {
                if(newText.Length > 0)
                {
                    newText += Environment.NewLine;
                }
                newText += "[nested_destination]" + this.DestinationFolderPath;
            }

           
            //TODO ?: at end of al tests, test if destination is inside source and add nested folder
            ReturnTypeExcludedFolders returnObject = new ReturnTypeExcludedFolders();
            returnObject.excludedFoldersText = newText;
            
            returnObject.errorsExcludedFoldersText = foundErrors;
            returnObject.lastTag = lastTag;
            returnObject.errorsVisible = errorsVisible;
            return returnObject;


        }

        public string BuildRobocopyCommand()
        {
            string RobocopyCommand = "Robocopy ";
            RobocopyCommand += "\"" + SourceFolderPath + "\" ";// source
            RobocopyCommand += "\"" + DestinationFolderPath + "\" "; // destination
            
            string exclFoldersXDPart = "/XD ";
            string exclFoldersPart = " ";
            bool exclFoldersPartNeeded = false;

            if (ExcludedFolders != "")
            {
               // string tempCommandPart = "/XD ";
                //string exclFolderPart = "";
                //bool tempCommandPartNeeded = false;
                // excluded directories / folders
                                          // transform ExcludedFolders to string[] and do iteration
                                          // transform cleanedText from string to string array 
                string[] seperators = new string[] { Environment.NewLine };
                string[] exclFoldersLines = ExcludedFolders.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                string patternNested = @"^\[nested_destination]";
                string replacement = @"";
                string pattern = @"^\[[^]]*]"; 
                foreach (String exclFolderLine in exclFoldersLines)
                {
                    if (exclFolderLine.Contains("[nested_destination]"))
                    {
                        string cleanedExclFolder = Regex.Replace(exclFolderLine, patternNested, replacement);
                        exclFoldersPart += "\"" + cleanedExclFolder + "\" ";
                        exclFoldersPartNeeded = true;
                        
                    }
                    else if(!Regex.IsMatch(exclFolderLine, pattern))
                    {
                        //string cleanedExclFolder = Regex.Replace(exclFolderLine, pattern, replacement);
                        exclFoldersPart += "\"" + exclFolderLine + "\" ";
                        exclFoldersPartNeeded = true;
                    }
                }
                //if (exclFoldersPartNeeded == true)
                //{
                //    RobocopyCommand += exclFoldersXDPart + exclFoldersPart;
                //}

            }

            if (exclFoldersPartNeeded == true)
            {
                RobocopyCommand += exclFoldersXDPart + exclFoldersPart;
            }
            

            //string tempCommandPart = "/XF ";
            string exclFilesXFPart = "/XF ";
            //bool tempCommandPartNeeded = false;
            bool exclFilesXFPartNeeded = false;
            string exclFilePart = "";
            if (ExcludedFiles != "")
            {
                //string tempCommandPart = "/XF ";
                //string exclFilePart = "";
                //bool tempCommandPartNeeded = false;

                string[] seperators = new string[] { Environment.NewLine };
                string[] exclFilesLines = ExcludedFiles.Split(seperators, StringSplitOptions.RemoveEmptyEntries);
                string replacement = @"";
                string pattern = @"^\[[^]]*]";

                foreach (String exclFileLine in exclFilesLines)
                {
                    if (!Regex.IsMatch(exclFileLine, pattern))
                    {
                        //string cleanedExclFile = Regex.Replace(exclFileLine, pattern, replacement);
                        exclFilePart += "\"" + exclFileLine + "\" ";
                        exclFilesXFPartNeeded = true;
                    }
                }
                //if (tempCommandPartNeeded == true)
                //{
                //    RobocopyCommand += tempCommandPart + exclFilePart;
                //}
            }
            if (exclFilesXFPartNeeded == true)
            {
                RobocopyCommand += exclFilesXFPart + exclFilePart;
            }


            string exclExtsPart = "";
            if (ExcludedExtensions != "")
            {
                string[] separators = new string[] { "," };
                string[] savedExts = ExcludedExtensions.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                List<string> savedExtsList = new List<string>();
                
                
                int counter = 0;
                if (savedExts.Length > 0)
                {
                    exclFilesXFPartNeeded = true;
                    foreach (string savedExt in savedExts)
                    {
                        //strip white space
                        savedExtsList.Add(savedExt.Trim());
                        
                        //exclExtsPart += " \"" + "*" + "." + savedExtsList[counter] + "\"";
                        exclExtsPart += " \"" + "*" + "." + savedExt + "\"";

                        counter += 1;
                    }

                }
                
            }

            if (exclFilesXFPartNeeded == true)
            {
                RobocopyCommand += exclFilesXFPart + exclFilePart + exclExtsPart +" ";
            }


            string modePattern = @"Git";
            string ModeItemStringCopy = ModeItemString;
            string RoboCopyOptionsCopy = RoboCopyOptions;
            
            if(ModeItemStringCopy == null) { ModeItemStringCopy = ""; }
            
            if (!Regex.IsMatch(ModeItemStringCopy, modePattern)); // robocopy options
            {
                if (ModeItemString == null)//  nothing choosen yet
                {
                    RoboCopyOptionsCopy = "/MIR /MT";
                } else if (ModeItemStringCopy == "Custom")
                {
                    ;
                } else
                {

                }
                if (ModeItemStringCopy == "") { ModeItemStringCopy = "Mirror"; }
               
                RobocopyCommand += RoboCopyOptionsCopy;

            }

            return RobocopyCommand;
        }

    }

}
